<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, miminum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <title>Captura</title>
  <style>
	body {
		color:#FFF;
		text-align:center;
		font-family:Arial, Helvetica, sans-serif;
		background:#dedede;
	}
	
  </style>
</head>
<body>

  <nav class="navbar navbar-default">
  <div class="container-fluid Color-Menu">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="" class="logo" /></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
        <!-- <li><a href="#">Link</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Opciones<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="FormularioCaptura.html">FormCaptura</a></li>
            <li><a href="servicios.html">Listado Servicios</a></li>
            <li><a href="editar.html">Edicion de Curso</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">

          <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
          <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ayuda <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Licencia</a></li>
            <li><a href="#">Asistecia</a></li>
            <li><a href="#">Creditos</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav> <!-- Barra de Navegacion -->
<div class="container">
	<div class="row">
      <div class="jumbotron text-center" id="jumbo-titulo">
        <h1>
          Captura de Datos
        </h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="panel panel-primary" >

          <div class="panel-body">
            <form class="form-vertical" role="form">
              <div class="col-md-6">
              	<div class="form-group">
              		<label for="nombre" >Nombre</label>
    				<input type="text" class="form-control" id="nombre" placeholder="Ejemplo: Juan José " />
              	</div>
              </div>
              <div class="col-md-6">
              	<div class="form-group">
              		<label for="apellidos" >Apellidos</label>
    				<input type="text" class="form-control" id="apellidos" placeholder="Ejemplo: Pérez Robles" />
              	</div>
              </div>
              <div class="form-group">
              		<label for="direccion" >Dirección</label>
    				<input type="text" class="form-control" id="direccion" placeholder="Ejemplo: Av. Siempre Viva #81" />
              </div>
              
              <div class="form-group">
                <label for="estado" >Estado</label>
                <div>
                  <select class="form-control" id="estado">
                    <option>Aguascalientes</option>
                    <option>Baja California</option>
                    <option>Baja California Sur</option>
                    <option>Campeche</option>
                    <option>Chiapas</option>
                    <option>Chihuahua</option>
                    <option>Ciudad de México</option>
                    <option>Coahuila de Zaragoza</option>
                    <option>Colima</option>
                    <option>Durango</option>
                    <option>Guanajuato</option>
                    <option>Guerrero</option>
                    <option>Hidalgo</option>
                    <option>Jalisco</option>
                    <option>México</option>
                    <option>Michoacán de Ocampo</option>
                    <option>Morelos</option>
                    <option>Nayarit</option>
                    <option>Nuevo León</option>
                    <option>Oaxaca </option>
                    <option>Puebla</option>
                    <option>Querétaro de Arteago</option>
                    <option>Quintana Roo</option>
                    <option>San Luís Potosí</option>
                    <option>Sinaloa</option>
                    <option>Sonora</option>
                    <option>Tabasco</option>
                    <option>Tamaulipas</option>
                    <option>Tlaxcala</option>
                    <option>Veracruz de Ignacio de la Llave</option>
                    <option>Yucatán</option>
                    <option>Zacatecas</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
              	<label for="ciudad" >Ciudad</label>
    			<input type="text" class="form-control" id="ciudad" placeholder="Ejemplo: Morelia" />
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label for="curso" >Curso</label>
                <div>
                  <select class="form-control" id="curso">
                    <option>Curso 1</option>
                    <option>Curso 42</option>
                    <option>Curso A</option>
                    <option>Otro curso</option>
                  </select>
                </div>
              </div>
              </div>
              <div class="col-md-6 ">
              <div class="form-group">
                <label for="instructor" >Instructor</label>
                <div>
                  <select class="form-control" id="instructor">
                    <option>Fulano de Tal</option>
                    <option>Mima Mame Mima</option>
                    <option>Foo bar poo</option>
                    <option>Sutano Mengano Perengano</option>

                  </select>
                </div>
              </div>
              </div>
              
              <div class="form-group">
              	  <div class="col-md-6">
              	  <button type="reset" class="btn btn-default" id="reestablecerDatos">Reestablecer</button>
              	  </div>
              	  <div class="col-md-6">
                  <button type="submit" class="btn btn-default" id="enviarDatos">Enviar</button>
                  </div>
                  

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>