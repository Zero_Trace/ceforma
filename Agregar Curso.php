<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, miminum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <title>Captura</title>
  <style>
	body {
		color:#FFF;
		text-align:center;
		font-family:Arial, Helvetica, sans-serif;
		background:#dedede;
	}
	
  </style>
</head>
<body>

  <nav class="navbar navbar-default">
  <div class="container-fluid Color-Menu">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="" class="logo" /></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
        <!-- <li><a href="#">Link</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Opciones<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="FormularioCaptura.html">FormCaptura</a></li>
            <li><a href="servicios.html">Listado Servicios</a></li>
            <li><a href="editar.html">Edicion de Curso</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">

          <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
          <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ayuda <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Licencia</a></li>
            <li><a href="#">Asistecia</a></li>
            <li><a href="#">Creditos</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav> <!-- Barra de Navegacion -->
<div class="container">
	<div class="row">
      <div class="jumbotron text-center" id="jumbo-titulo">
        <h1>
          Captura de Datos
        </h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="panel panel-primary" >

          <div class="panel-body">
            <form action="insertarcurso.php" class="form-vertical" role="form"  method="POST">
              <div class="col-md-6">
              	<div class="form-group">
              		<label for="idcurso" >Id Curso</label>
    				<input type="text" class="form-control" id="idcurso" placeholder="Ejemplo:12365 " />
              	</div>
              </div>
              <div class="col-md-6">
              	<div class="form-group">
              		<label for="nombrecurso" >Nombre del curso</label>
    				<input type="text" class="form-control" id="nombrecurso" placeholder="Ejemplo: Jardineria" />
              	</div>
              </div>
              <div class="form-group">
              		<label for="duracion" >Duracion en minutos(60= 1hora)</label>
    				<input type="text" class="form-control" id="duracion" placeholder="Ejemplo:60" />
              </div>

              <div class="form-group">
                <label for="area" >Area</label>
                <div>
                  <select class="form-control" id="area">
                    <option>Administracion</option>
                    <option>Desarrollo Humano</option>
                    <option>Alta Direccion</option>
                    <option>Tecnologias de la Informacion</option>
                    <option>Gastronomia</option>
                    <option>Desarrollo Organizacional</option>
                    <option>Otros</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
              	<label for="costo" >Costo en pesos mx</label>
    			<input type="text" class="form-control" id="costo" placeholder="Ejemplo: 2000" />
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label for="capacidad" >Capacidad (Personas)</label>
                <input type="text" class="form-control" id="capacidad" placeholder="Ejemplo:10" />
                </div>
              </div>


              <div class="form-group">
              	  <div class="col-md-6">
              	  <button type="reset" class="btn btn-default" id="reestablecerDatos">Reestablecer</button>
              	  </div>
              	  <div class="col-md-6">
                  <button type="submit" class="btn btn-default" id="enviarDatos">Enviar</button>
                  </div>


              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
